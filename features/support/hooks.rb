require "selenium-webdriver"
require "report_builder"

Before do |scn|
  @Methods = Methods.new
  Capybara.current_session.driver.browser.manage.delete_all_cookies
  page.driver.browser.manage.window.maximize
end

After do |scenario|
  # binding.pry

  if scenario.failed?
    #binding.pry
  end

  Capybara.current_session.driver.browser.manage.delete_all_cookies
  Capybara.current_session.driver.quit
end
