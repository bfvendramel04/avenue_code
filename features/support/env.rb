require "rspec"
require "yaml"
require "pry"
require "capybara/cucumber"
require "capybara/poltergeist"

ELEMENTS = YAML.load_file("data/elements.yml")
DATA = YAML.load_file("data/data.yml")

#configuração dos navegadores
if ENV["chrome"]
  Capybara.default_driver = :chrome
  Capybara.register_driver :chrome do |app|
    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps["pageLoadStrategy"] = "none"
    Capybara::Selenium::Driver.new(app, browser: :chrome, port: 9922, desired_capabilities: caps, switches: ["--disable-web-security", "--reduce-security-for-testing", "--repl", "--incognito"])
  end
elsif ENV["chrome_headless"]
  Capybara.default_driver = :chrome_headless
  Capybara.register_driver :chrome_headless do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome, switches: ["--incognito", "--headless", "disable-gpu", "--start-maximized", "--no-sEbox"])
  end
elsif ENV["remote_headless"]
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    "chromeOptions" => { "args" => ["--fast", "--no-default-browser-check", "pageLoadStrategy=none", "--no-sEbox", "--headless", "disable-gpu", "window-size=1280x720", "--proxy-server=#{$proxy}"] },
  )
  capabilities["browserName"] = "chrome"
  capabilities["platform"] = "WINDOWS"
  capabilities["pageLoadStrategy"] = "none"
  Capybara.default_driver = :chrome_headless
  Capybara.register_driver :chrome_headless do |app|
    Capybara::Selenium::Driver.new(app,
                                   browser: :chrome,
                                   url: "http:",
                                   desired_capabilities: capabilities)
  end
elsif ENV["firefox"]
  Capybara.default_driver = :firefox
  Capybara.register_driver :firefox do |app|
    Capybara::Selenium::Driver.new(app, browser: :firefox)
  end
elsif ENV["ie"]
  Capybara.default_driver = :ie
  Capybara.register_driver :ie do |app|
    Capybara::Selenium::Driver.new(app, browser: :internet_explorer)
  end
elsif ENV["headless_debug"]
  Capybara.default_driver = :poltergeist_debug
  Capybara.register_driver :poltergeist_debug do |app|
    Capybara::Poltergeist::Driver.new(app, inspector: true)
  end
  Capybara.javascript_driver = :poltergeist_debug
elsif ENV["headless"]
  Capybara.javascript_driver = :poltergeist
  Capybara.default_driver = :poltergeist
else
  Capybara.default_driver = :selenium
end
Capybara.default_max_wait_time = 20
