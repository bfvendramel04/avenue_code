class Methods
  include RSpec::Matchers
  include Capybara::DSL

  def access_and_login
    visit(DATA["url"])
    find(ELEMENTS["sign_in"], wait: 40).click
    find(ELEMENTS["email"]).click.set(DATA["email"])
    find(ELEMENTS["password"]).click.set(DATA["password"])
    find(ELEMENTS["btn_sign_in"]).click
  end

  def signed_in_ok
    has_selector?(ELEMENTS["signIn_ok"], text: "Signed in successfully")
  end

  def my_task_page
    first(ELEMENTS["my_task"]).click
    has_selector?(ELEMENTS["my_task_panel"], text: "To be Done")
  end

  def my_task_set_massage
    name = "Bruno"
    msg = find(:xpath, "/html/body/div[1]/h1").text
    if msg == "Hey #{name}, this is your todo list for today"
      puts "SUCCESS: Message format corret"
    else
      raise "ERROR: Message format incorret"
    end
  end

  def add_task(add_task)
    case add_task
    when "click"
      $task_name = "Adding task by clicking on Add button"
      find(ELEMENTS["my_task_field"]).click.set($task_name)
      find(ELEMENTS["btn_add_task"]).click
    when "enter"
      $task_name = "Adding task by hitting ENTER"
      find(ELEMENTS["my_task_field"]).click.set($task_name)
      find(ELEMENTS["my_task_field"]).native.send_keys(:enter)
    end
  end

  def check_added_task
    within(ELEMENTS["task_table"]) do
      if first(ELEMENTS["added_task_field"]).find(ELEMENTS["added_task_text"]).text == $task_name
        puts "Task added successfuly"
      else
        raise "Tasked adding failed"
      end
    end
  end

  def task_characters_field(characters)
    $characters = characters
    task_description = "ThisTaskIsTaskIsMeantToCountTheMinimumAndMaximumCharactersTheTaskFieldCanSupportSoIAmGoingToKeepTypingThisUntilIReachItsMaximum. ThisTaskIsTaskIsMeantToCountTheMinimumAndMaximumCharactersTheTaskFieldCanSupportSoIAmGoingToKeepTypingThisUntilIReachItsMaximum."
    find(ELEMENTS["my_task_field"]).click.set(task_description[0..characters.to_i - 1])
    $task_name = task_description[0..characters.to_i - 1]
    find(ELEMENTS["my_task_field"]).native.send_keys(:enter)
    puts "Tasked added"
  end

  def check_added_task_by_characters
    length = find(ELEMENTS["task_table"]).first(ELEMENTS["added_task_field"]).find(ELEMENTS["added_task_text"]).text.length

    if length.to_s == $characters
      puts "Number of characters accepted as #{$characters}"
    else
      puts "Field didnt accept more than #{length} characters"
    end

    if length.to_i >= 3 && length.to_i <= 250
      puts "SUCCESS: Minimum and Maximum number of characters is correct"
      if length.to_i < 3 && length.to_i > 250
        raise "ERROR: Minimum and Maximum number of characters is incorrect"
      end
    end
  end

  #subtasks
  def assert_subtask_btn(subtask_btn)
    $first_task = find(ELEMENTS["task_table"]).first(ELEMENTS["added_task_field"])
    $first_task.has_selector?(ELEMENTS["btn_mng_subtask"], text: subtask_btn)
  end

  def assert_subtask_number
    subtask_number = $first_task.find(ELEMENTS["btn_mng_subtask"]).text
    subtask_number.delete!("(").delete!(") Manage Subtasks")
    subtask_number = eval(subtask_number)
    puts "The number of subtasks for this task is #{subtask_number}"
  end

  def click_subtask_btn(subtask_btn)
    $first_task = find(ELEMENTS["task_table"]).first(ELEMENTS["added_task_field"])
    $first_task.find(ELEMENTS["btn_mng_subtask"], text: subtask_btn).click
  end

  def assert_subtask_modal
    has_selector?(ELEMENTS["subtask_modal"])
  end

  def subtask_id
    subtask_ID = find("div[class='modal-header ng-scope']").text
    subtask_ID.delete!("Editing Task ")
    puts "Subtask ID is #{subtask_ID}"
    has_selector?(ELEMENTS["subtask_task_desc_field"])
  end
end
