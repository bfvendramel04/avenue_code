Entao("I expect to be redirected to My ToDo list page") do
  @Methods.signed_in_ok
end

Quando("I click on {string}") do |myTask|
  @Methods.my_task_page
end

Quando("I am on my ToDo list page") do
  @Methods.my_task_page
end

Entao("I expect to have the set message displayed") do
  @Methods.my_task_set_massage
end

Quando("I type a task description in new taks field and hit to {string}") do |add_task|
  @Methods.add_task(add_task)
end

Entao("the task should be appended on the list of created tasks") do
  @Methods.check_added_task
end

Quando("I type a task description in task field with {string} characters and hit to add task") do |characters|
  @Methods.task_characters_field(characters)
end

Entao("I assert if taks was properly added") do
  @Methods.check_added_task_by_characters
end
