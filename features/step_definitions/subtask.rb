Quando("I assert the visibility of {string} button") do |subtask_btn|
  @Methods.assert_subtask_btn(subtask_btn)
end

Entao("I should also assert the number of subtasks assigned to this task") do
  @Methods.assert_subtask_number
end

Quando("I click the {string} button") do |subtask_btn|
  @Methods.click_subtask_btn(subtask_btn)
end

Quando("the modal dialog opens up") do
  @Methods.assert_subtask_modal
end

Entao("This popup should have a read only field with the task ID and the task description") do
  binding.pry
end

Quando("I enter the SubTask Description in subtask field with {string} charactersandSubTask due date {string}") do |string, string2|
  binding.pry
end

Quando("I click on the add button") do
  binding.pry
end

Entao("Subtasks that were added should be appended on the bottom part of the modal dialog") do
  binding.pry
end
