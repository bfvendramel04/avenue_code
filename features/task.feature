#language: pt

Funcionalidade: Createandmanage a task
   As a ToDo App user I should be able to create a task So I can manage my tasks

Contexto: Access and login 
 Dado I am signed in ToDo App

@1
Cenario:  Validate My Task link
Quando I click on "My Task"
Entao I expect to be redirected to My ToDo list page

@2
Cenario: Validate the ToDo list message
Quando I am on my ToDo list page
Entao I expect to have the set message displayed

@3
Esquema do Cenario: Adding a new task
E I am on my ToDo list page
Quando I type a task description in new taks fieldandhit to "<add_task>"
Entao the task should be appended on the list of created tasks

Exemplos:
| add_task | 
| enter    |
| click    |

@4
Esquema do Cenario: Minimum characters lenght to enter a task
E I am on my ToDo list page
Quando I type a task description in task field with "<characters>" characters and hit to add task
Entao I assert if taks was properly added

Exemplos:
| characters  | 
| 1           |
| 2           |
| 3           |
| 4           |


@5
Esquema do Cenario: Maximum characters lenght to enter a task
E I am on my ToDo list page
Quando I type a task description in task field with "<characters>" characters and hit to add task
Entao I assert if taks was properly added

Exemplos:
| characters  | 
| 249         |
| 250         |
| 254         |
| 255         |



