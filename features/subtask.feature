#language: pt

Funcionalidade: sCreateandmanage a Subtask
   As a ToDo App user I should be able to create a subtask

Contexto: Access and login 
 Dado I am signed in ToDo App
 E I am on my ToDo list page

@6
Cenario: Validate Manage Subtasks Button
Quando I assert the visibility of "Manage Subtasks" button
Entao I should also assert the number of subtasks assigned to this task

@7
Cenario: Validate Subtasks fields
Quando I click the "Manage Subtasks" button
E the modal dialog opens up
Entao This popup should have a read only field with the task ID and the task description

@8
Esquema do Cenario: Adding a Substask
E I click the "Manage Subtasks" button
Quando I enter the SubTask Description in subtask field with "<number>" charactersandSubTask due date "<dt_format>"
E I click on the add button
Entao Subtasks that were added should be appended on the bottom part of the modal dialog

Exemplos:
| number | dt_format  |
| 249    | 04/29/2019 |
| 250    | 29/04/2019 |
| 254    | 2019/04/29 |
| 255    | 2019/29/04 |